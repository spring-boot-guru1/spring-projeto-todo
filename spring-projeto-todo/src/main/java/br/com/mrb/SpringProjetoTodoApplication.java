package br.com.mrb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringProjetoTodoApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringProjetoTodoApplication.class, args);
	}

}
