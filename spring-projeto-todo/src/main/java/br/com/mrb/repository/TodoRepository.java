package br.com.mrb.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import br.com.mrb.model.Todo;

public interface TodoRepository extends MongoRepository<Todo, String>{

}
