package br.com.mrb.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import br.com.mrb.repository.TodoRepository;

@Component
public class DataInitialize implements ApplicationListener<ContextRefreshedEvent>{

	@Autowired
	TodoRepository todoRepository;
	
	@Override
	public void onApplicationEvent(ContextRefreshedEvent event) {
		
//		List<Todo> todos = todoRepository.findAll();
//		
//		if(todos!=null && !todos.isEmpty()) {
//			
//			System.out.println("Data Initialaze");
//			Todo  t1 = new Todo();
//			t1.setDescription("Flamengo campeão");
//			t1.setDone(true);
//			t1.setCreatedAt(new Date());
//			todoRepository.save(t1);
//			
//			Todo  t2 = new Todo();
//			t2.setDescription("Vasco rebaixado");
//			t2.setDone(true);
//			t2.setCreatedAt(new Date());
//			todoRepository.save(t2);
//			
//			Todo  t3 = new Todo();
//			t3.setDescription("Botafogo rebaixado");
//			t3.setDone(true);
//			t3.setCreatedAt(new Date());
//			todoRepository.save(t3);
//			
//			Todo  t4 = new Todo();
//			t4.setDescription("Fluminense pague segunda divisão");
//			t4.setDone(true);
//			t4.setCreatedAt(new Date());
//			todoRepository.save(t4);
//		}
	}

}
