package br.com.mrb.controller;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.mrb.model.Todo;
import br.com.mrb.repository.TodoRepository;

@CrossOrigin("*")
@RestController
@RequestMapping("/api")
public class TodoController {

	@Autowired
	private TodoRepository todoRepository;
	
	@RequestMapping(value = "/todos",  method = RequestMethod.GET)
	public List<Todo> findAll(){
		return todoRepository.findAll();
	}
	
	@RequestMapping(value = "/todos/{id}",  method = RequestMethod.GET)
	public Todo findById(@PathVariable String id) {
		return todoRepository.findById(id).get();
	} 
	
	
	@RequestMapping(value = "/todos",  method = RequestMethod.POST)
	public Todo save(@RequestBody Todo t) {
		t.setCreatedAt(new Date());
		t.setDone(false);
		return todoRepository.save(t);
	}
	
	@RequestMapping(value = "/todos",  method = RequestMethod.PUT)
	public Todo update(@RequestBody Todo todo) {
		return todoRepository.save(todo);
	}
	
	
	@RequestMapping(value = "/todos/{id}",  method = RequestMethod.DELETE)
	public void delete(@PathVariable String id) {
		try {
			todoRepository.deleteById(id);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
